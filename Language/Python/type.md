Python中一切皆是对象，类可以实例化为实例，同时类也是metaclass的实例化

---
python中类的概念，是借鉴自smalltalk语言。

在大部分语言中，类指的是"描述如何产生一个对象(object)"的一段代码，这对于python也是如此。  

但是,在python中，类远不止如此，类同时也是对象。

当你遇到关键词class的时候，python就会自动执行产生一个对象。下面的代码段中:
```python
>>> class ObjectCreator(object):
         pass
```
python在内存中产生了一个名叫做"ObjectCreator"的对象。这个对象(类)自身拥有产生对象(实例instance)的能力。 这就是为什么称呼这东西(后面遇到容易混淆的地方,我们称之为:类对象)也是类的原因。同时，它也是一个对象，因此你可以对它做如下操作:
- 赋值给变量
- 复制它
- 为它增加属性(attribute)
- 作为参数传值给函数  

举例：
```python
>>> print(ObjectCreator) # 你可以打印一个类,因为它同时也是对象
<class '__main__.ObjectCreator'>

>>> def echo(o):
...     print(o)
...
>>> echo(ObjectCreator) # 作为参数传值给函数
<class '__main__.ObjectCreator'>

>>> print(hasattr(ObjectCreator, 'new_attribute'))
False
>>> ObjectCreator.new_attribute = 'foo' # you can add attributes to a class
>>> print(hasattr(ObjectCreator, 'new_attribute'))
True
>>> print(ObjectCreator.new_attribute)
foo

>>> ObjectCreatorMirror = ObjectCreator # 将类赋值给变量
>>> print(ObjectCreatorMirror.new_attribute)
foo
>>> print(ObjectCreatorMirror())
<__main__.ObjectCreator object at 0x8997b4c>
```
## 动态创建类
---
既然类也是对象，那就应该有用来产生它的东西。这东西就是type。

先来说说你所认识的type。这个古老而好用的函数，可以让我们知道一个对象的类型是什么。

```python
>>> print(type(1))
<type 'int'>
>>> print(type("1"))
<type 'str'>
>>> print(type(ObjectCreator))
<type 'type'>
>>> print(type(ObjectCreator()))
<class '__main__.ObjectCreator'>
```

实际上，type还有一个完全不同的功能，它可以在运行时产生类。type可以传入一些参数，然后返回一个类。(好吧，必须承认，根据不同的传入参数，一个相同的函数type居然会有两个完全不同的作用，这很愚蠢。不过python这样做是为了保持向后兼容性。)

下面举例type创建类的用法。首先，对于类一般是这么定义的:

```python
>>> class MyShinyClass(object):
        pass
```
在下面，MyShinyClass也可以这样子被创建出来,并且跟上面的创建方法有一样的表现:
```
>>> MyShinyClass = type('MyShinyClass', (), {}) # returns a class object
>>> print(MyShinyClass)
<class '__main__.MyShinyClass'>
>>> print(MyShinyClass()) # create an instance with the class
<__main__.MyShinyClass object at 0x8997cec>
```
type创建类需要传入三个参数,分别为name、bases、dict:

- 类的名字
- 一组"类的父类"的元组(tuple) (这个会实现继承,也可以为空)
- 字典 (类的属性名与值,key-value的形式，不传相当于为空，如一般写法中的pass).

下面来点复杂的，来更好的理解type传入的三个参数:
```python
class Foo(object):
    bar = True

    def echo_bar(self):
        print(self.bar)
```
等价于：
```python 
def echo_bar(self):
    print(self.bar)

Foo = type('Foo', (object,), {'bar':True, 'echo_bar': echo_bar})
# python 元组当只有1个元素时，不可省略逗号
```
```python
class FooChild(Foo):
    pass
```
等价于：
```python
FooChild = type('FooChild', (Foo, ), {})
```
在python中，类就是对象，并且你可以在运行的时候动态创建类.




### cls and self

> Before diving into the actual implementations you need to know that `__new__` accepts cls as it's first parameter and `__init__` accepts self, because when calling `__new__` you actually don't have an instance yet, therefore no self exists at that moment, whereas `__init__` is called after `__new__` and the instance is in place, so you can use self with it.



---
### difference between `type()` and `type.__new__()`


[链接](https://stackoverflow.com/questions/2608708/what-is-the-difference-between-type-and-type-new-in-python)