## new-style class and old-style class
New-style classes inherit from object, or from another new-style class.
```python
class NewStyleClass(object):
    pass

class AnotherNewStyleClass(NewStyleClass):
    pass
```
Old-style classes don't.
```python
class OldStyleClass():
    pass
```
**Python 3 Note:**

Python 3 doesn't support old style classes, so either form noted above results in a new-style class.
```python
class OldStyleClass():
    pass

#In Python3.X is equal to 

class OldStyleClass(object):
    pass
```


