`isinstance(object, classinfo)`

-   object -- 实例对象。
-   classinfo -- 可以是直接或间接类名、基本类型或者由它们组成的元组。

> 当object的类型与classinfo相同或与元组中一个相同，返回`True`

---
与`type()`的区别：
-   `type()` 不会认为子类是一种父类类型，不考虑继承关系。
    
-   `isinstance()`会认为子类是一种父类类型，考虑继承关系。
