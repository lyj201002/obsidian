[[Modules]]作为脚本运行时`__name__`变量被设为`__main__`，当[[Modules]]被`import`到另一个[[Modules]]时`__name__`被设为原来的模块名

```
#module_one.py
print("This is ", __name__)
if __name__ == '__main__':
	print("This is ", __name__)
```
> 运行module_one.py输出为："This is __main__"
```
#module_two.py
import module_one
```
> 运行module_two.py输出为："This is module_one"

