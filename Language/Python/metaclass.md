## Metaclass
很多书都会翻译成 元类，仅从字面理解， meta 的确是元，本源，翻译没毛病。但理解时，应该把元理解为描述数据的超越数据，事实上，metaclass 的 meta 起源于希腊词汇 meta，包含两种意思：

- “Beyond”，例如技术词汇 metadata，意思是描述数据的超越数据。
- “Change”，例如技术词汇 metamorphosis，意思是改变的形态。
因此可以理解为 metaclass 为描述类的超类，同时可以<u>改变子类的形态</u>。



**Thomas' description of meta-classes here is excellent:**
> A metaclass is the class of a class. Like a class defines how an instance of the class behaves, a metaclass defines how a class behaves. A class is an instance of a metaclass.
---
metaclass 就是创建类的那家伙。(事实上，type就是一个metaclass)

我们知道,我们定义了class就是为了能够创建object的，没错吧?

我们也学习了，python中类也是对象。

那么，metaclass就是用来创造“类对象”的类.它是“类对象”的“类”。

可以这样子来理解:
![[img1.png]]
python在背后使用type创造了所有的类。type是所有类的metaclass.

我们可以使用__class__属性来验证这个说法.

在python中，一切皆为对象：整数、字符串、函数、类.所有这些对象，都是通过类来创造的.
```python
>>> age = 35
>>> age.__class__
<type 'int'>

>>> name = 'bob'
>>> name.__class__
<type 'str'>

>>> def foo(): pass
>>> foo.__class__
<type 'function'>

>>> class Bar(object): pass
>>> b = Bar()
>>> b.__class__
<class '__main__.Bar'>
```
type是python內置的metaclass。不过，你也可以编写自己的metaclass.

我们可以在一个类中加入 metaclass 属性.
```python
class Foo(metaclass=something):
    ......  # 省略
```

当你这么做了，python就会使用metaclass来创造类:Foo。

当你写下class Foo(metaclass)的时候，类对象Foo还没有在内存中生成。

python会在类定义中寻找metaclass 。如果找到了，python就会使用这个metaclass 来创造类对象: Foo。如果没有在Foo中找到metaclass，它会继续在Bar（父类）中寻找metaclass，并尝试做和前面同样的操作。

如果python由下往上遍历父类也都没有找不到metaclass，它就会在模块(module)中去寻找metaclass，并尝试做同样的操作。

如果还是没有找不到metaclass， 才会用内置的type(这也是一个metaclass)来创建这个类对象。

## 自定义metaclass

使用metaclass的主要目的，是为了能够在创建类的时候，自动地修改类。

一个很傻的需求，我们决定要将该模块中的所有类的属性，改为大写。

```python
# type也是一个类，我们可以继承它.
class UpperAttrMetaclass(type):
    # __new__ 是在__init__之前被调用的特殊方法
    # __new__是用来创建对象并返回这个对象
    # 而__init__只是将传入的参数初始化给对象
    # 实际中,你很少会用到__new__，除非你希望能够控制对象的创建
    # 在这里，类是我们要创建的对象，我们希望能够自定义它，所以我们改写了__new__
    # 如果你希望的话，你也可以在__init__中做些事情
    # 还有一些高级的用法会涉及到改写__call__，但这里我们就先不这样.

    def __new__(cls, name,bases, dict):
        uppercase_attr = {}
        for name, val in dict.items():
            if not name.startswith('__'):
                uppercase_attr[name.upper()] = val
            else:
                uppercase_attr[name] = val
        return type(name, bases, uppercase_attr)
```
这里的方式其实不是OOP(面向对象编程).因为我们直接调用了type,而不是改写父类的__type__方法.

所以我们也可以这样子处理:
```python
class UpperAttrMetaclass(type):
    def __new__(cls, name,bases, dict):
        uppercase_attr = {}
        for name, val in dict.items():
            if not name.startswith('__'):
                uppercase_attr[name.upper()] = val
            else:
                uppercase_attr[name] = val
        return type.__new__(cls, name,
                            bases, uppercase_attr)
```
注意到__new__方法相比于type()函数多了一个参数cls(类本身)，即创建了一个cls类

(我们同时使用常见的super来让代码更清晰)
```python
class UpperAttrMetaclass(type):

    def __new__(cls, clsname, bases, attrs):
        uppercase_attr = {}
        for name, val in attrs.items():
            if not name.startswith('__'):
                uppercase_attr[name.upper()] = val
            else:
                uppercase_attr[name] = val
        return super(UpperAttrMetaclass, cls).__new__(cls, clsname, bases, uppercase_attr)
```
