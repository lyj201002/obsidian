每个python文件都是module，文件名由模块名+扩展名构成，模块名可由[[__name__]]变量获得

模块可直接运行，也可在import后运行(**首次**import时，module内的可执行部分会被执行)

查看当前已引用模块可使用
```
import sys
sys.modules.get(search-name, didn't-find-info)
```
## More on Modules
### The Module Search Path
`import` 模块时，首先会在python `built-in` 模块中寻找，若找不到对应name，则在`sys.path`变量中寻找

`sys.path`初始包含：
- 脚本运行目录(若由命令行唤醒解释器则为命令行当前目录)
- PYTHONPATH(搜索路径，环境变量)
-  installation-dependent default


### “Compiled” Python files
为加速模块加载，Python把模块编译版缓存在`__pycache__`目录中，文件名为`module._version_.pyc`,version 对编译文件格式进行编码，一般是 Python 的版本号。例如，CPython 的 3.3 发行版中，spam.py 的编译版本缓存为 `__pycache__/spam.cpython-33.pyc`。使用这种命名惯例，可以让不同 Python 发行版及不同版本的已编译模块共存。

### Package
Python只把含`__path__`属性的模块视为package。Package分为，
- Regular packages(typically implemented as a directory containing  `__init__.py`)
- Namespace packages

Regular packages包含`__init__.py`文件，该文件可以空文件，也可执行package的初始化或设置`__all__`变量

从包中导入单个模块，例如：

`import sound.effects.echo`

这段代码加载子模块 `sound.effects.echo` ，但引用时必须使用子模块的全名：

`sound.effects.echo.echofilter(input, output, delay\=0.7, atten\=4)`

另一种导入子模块的方法是 ：

`from sound.effects import echo`

这段代码还可以加载子模块 `echo` ，并且不加包前缀也可以使用。因此，可以按如下方式使用：

`echo.echofilter(input, output, delay\=0.7, atten\=4)`

另一种变体是直接导入所需的函数或变量：

`from sound.effects.echo import echofilter`
同样，这样也会加载子模块 `echo`，但可以直接使用函数 `echofilter()`：

`echofilter(input, output, delay\=0.7, atten\=4)`

> 注意，使用 `from package import item` 时，item 可以是包的子模块（或子包），也可以是包中定义的函数、类或变量等其他名称。`import` 语句首先测试包中是否定义了 item；如果未在包中定义，则假定 item 是模块，并尝试加载。如果找不到 item，则触发 `ImportError`异常。
> 相反，使用 `import item.subitem.subsubitem` 句法时，除最后一项外，**每个 item 都必须是包；最后一项可以是模块或包，但不能是上一项中定义的类、函数或变量。**

使用 `from x import item` 时, Python 需要先判断 x 是否为package，而判断条件就是 x 是否具有 `__path__`属性 ，不论`__path__`的值如何。例子如下

> Python中模块变量\_\_path\_\_在这段代码中怎么传进来的？ - denglj的回答 - 知乎 https://www.zhihu.com/question/67447987/answer/253317374
#### Importing \* From a Package

使用 `from sound.effects import *` 时，该语句在文件系统查找并导入包的所有子模块。这项操作花费的时间较长，并且导入子模块可能会产生不必要的副作用。

唯一的解决方案是提供包的显式索引。如果包的 `__init__.py` 代码定义了列表 `__all__`，运行 `from package import *` 时，则导入列表中的模块。