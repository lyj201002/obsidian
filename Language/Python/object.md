## Object vs. type

![[img2.png]]

```python
#type is kind of object,即Type是object的子类
>>> type.__class__
<class 'type'> # type是type的一个实例，即type是type类
>>> type.__bases__
(<class 'object'>,) # type的父类是object

#object is and instance of type,即Object是type的一个实例
>>> object.__class__
<class 'type'> # object是type的一个实例，即object是type类
>>> object.__bases__ 
() # object 无父类，因为它是链条顶端
```

![[img3.png]]
> 虚线表示源是目标的实例，实线表示源是目标的子类。即，左边的是右边的类型，而上面的是下面的父亲。