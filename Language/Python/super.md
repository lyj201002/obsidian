### TLDR
> Python 3 可以使用直接使用 super().xxx 代替 super(Class, self).xxx 
---

### MRO
> Method Resolution Order
super()可以用于调用父类(超类)的某个方法，主要用在多继承中，在单继承时直接调用父类方法即可，但是在多继承中就会涉及到重复继承等问题，这里也需要牵涉到MRO

对类对象调用`mro()`方法即可查看继承属性

**在 MRO 中，基类永远出现在派生类后面，如果有多个基类，基类的相对顺序保持不变。**


```python
class A(object):
    def __init__(self):
        print('enter A')
        print('leave A')

class B(object):
    def __init__(self):
        print('enter B')
        print('leave B')

class C(A):
    def __init__(self):
        print('enter C')
        super(C, self).__init__()
        print('leave C')

class D(A):
    def __init__(self):
        print('enter D')
        super(D, self).__init__()
        print('leave D')

class E(B, C):
    def __init__(self):
        print('enter E')
        super(E, self).__init__()
        print('leave E')

class F(E, D):
    def __init__(self):
        print('enter F')
        super(F, self).__init__()
        print('leave F')

f = F()
print(F.__mro__)
```
```mermaid
    graph TD
    o[Object]-->a[A]-->c[C]
    o[Object]-->b[B]-->e[E]
    a-->d[D]-->f[F]
    e-->f
```

mro为
```python
(<class '__main__.F'>,
 <class '__main__.E'>,
 <class '__main__.B'>,
 <class '__main__.C'>,
 <class '__main__.D'>,
 <class '__main__.A'>,
 <class 'object'>)
```

### [super][1]

super()可理解为

```python
def super(cls, inst):
    mro = inst.__class__mro()
    return mro(mro.index(cls) + 1)
``` 
super做了两件事情，获取mro列表，然后返回列表中下一个类

### `__call__` Pseudo code

**[Therefore if type is the parent of Meta_1 we can imagine a pseudo implementation of type.`__call__()` as such][2]**
```python
class type:
    def __call__(cls, *args, **kwarg):

        # ... a few things could possibly be done to cls here... maybe... or maybe not...

        # then we call cls.__new__() to get a new object
        obj = cls.__new__(cls, *args, **kwargs)

        # ... a few things done to obj here... maybe... or not...

        # then we call obj.__init__()
        obj.__init__(*args, **kwargs)

        # ... maybe a few more things done to obj here

        # then we return obj
        return obj
```
---
[^_^]: 链接

[1]:(https://rhettinger.wordpress.com/2011/05/26/super-considered-super/)
[2]:https://stackoverflow.com/questions/6966772/using-the-call-method-of-a-metaclass-instead-of-new/39363704?noredirect=1#comment100012101_39363704