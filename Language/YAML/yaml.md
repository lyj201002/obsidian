YAML Ain't a Markup Language
---
## TLDR
- 大小写敏感
- 使用缩进表示层级关系
- 缩进不允许使用tab，只允许空格
- 缩进的空格数不重要，只要相同层级的元素左对齐即可
- '#'表示注释
---
YAML 支持的数据结构有三种。
- 对象：键值对的集合，又称为映射（mapping）/ 哈希（hashes） / 字典（dictionary）
- 数组：一组按次序排列的值，又称为序列（sequence） / 列表（list）
- 纯量（scalars）：单个的、不可再分的值
 ## 对象
对象的一组键值对，使用冒号结构表示。   
```
animal: pets
-->
{ animal: 'pets' }


hash: { name: Steve, foo: bar } 
-->
{ hash: { name: 'Steve', foo: 'bar' } }

```
## 数组
一组连词线开头的行，构成一个数组。
```
- Cat
- Dog
- Goldfish
-->
[ 'Cat', 'Dog', 'Goldfish' ]


-
 - Cat
 - Dog
 - Goldfish
 -->
 [ [ 'Cat', 'Dog', 'Goldfish ' ] ]
```
## 文本块
- 使用 `|` 和文本内容缩进表示的块：保留块中已有的回车换行。相当于段落块
```
# 注意 ":" 与 "|" 之间的空格
yaml: |
   JSON的语法其实是YAML的子集，大部分的JSON文件都可以被YAML的解释器解释。
   如果你想更深入的学习YAML，我建议你去 http://www.yaml.org 看看
```
- 使用 `>`和文本内容缩进表示的块：将块中回车替换为空格，最终连接成一行

```
# 注意 ":" 与 ">" 之间的空格，另外可以使用空行来分段落
yaml: >
   JSON的语法其实是YAML的子集，
   大部分的JSON文件都可以被YAML的解释器解释。
   如果你想更深入的学习YAML，我建议你去 http://www.yaml.org 看看
```

- 使用定界符`""`、`''`或`回车`表示的块：最终表示成一行

```
yaml:     # 使用回车的多行，最终连接成一行。
   JSON的语法其实是YAML的子集，
   大部分的JSON文件都可以被YAML的解释器解释。
yaml:     # 使用了双引号，双引号的好处是可以转义，即在里面可以使用特殊符号
   "JSON的语法其实是YAML的子集，
   大部分的JSON文件都可以被YAML的解释器解释。"
```

- 当数据中含有空格或任意特殊字符，需要使用引号来包裹任何包含冒号的哈希值
## 复合结构
```
languages:
 - Ruby
 - Perl
 - Python 
websites:
 YAML: yaml.org 
 Ruby: ruby-lang.org 
 Python: python.org 
 Perl: use.perl.org 
 -->
 { languages: [ 'Ruby', 'Perl', 'Python' ],
  websites: 
   { YAML: 'yaml.org',
     Ruby: 'ruby-lang.org',
     Python: 'python.org',
     Perl: 'use.perl.org' } }
```
## 纯量
- 字符串
- 布尔值
- 整数
- 浮点数
- Null
- 时间
- 日期
```
数值
number: 12.30
-->
{ number: 12.30 }


布尔值
isSet: true
-->
{ isSet: true }


null
parent: ~ 
-->
{ parent: null }


时间
采用 ISO8601 格式。
iso8601: 2001-12-14t21:59:43.10-05:00 
-->
{ iso8601: new Date('2001-12-14t21:59:43.10-05:00') }

采用复合 iso8601 格式的年、月、日表示。
date: 1976-07-31
-->
{ date: new Date('1976-07-31') }



强制类型转换 !!
e: !!str 123
f: !!str true
-->
{ e: '123', f: 'true' }
```

## 字符串
默认不使用引号表示， 当字符串中由空格或特殊字符，需要引号  
单引号会转义， 双引号不转义

```

s1: '内容\n字符串'
s2: "内容\n字符串"
-->
{ s1: '内容\\n字符串', s2: '内容\n字符串' }
```
单引号之中如果还有单引号，必须连续使用两个单引号转义。

```
str: 'labor''s day' 
-->
{ str: 'labor\'s day' }
```
## 引用
锚点&和别名*，可以用来引用。&用来建立锚点（defaults），<<表示合并到当前数据，*用来引用锚点。
```
defaults: &defaults
  adapter:  postgres
  host:     localhost

development:
  database: myapp_development
  <<: *defaults

test:
  database: myapp_test
  <<: *defaults


-->


defaults:
  adapter:  postgres
  host:     localhost

development:
  database: myapp_development
  adapter:  postgres
  host:     localhost

test:
  database: myapp_test
  adapter:  postgres
  host:     localhost




- &showell Steve 
- Clark 
- Brian 
- Oren 
- *showell 
-->
[ 'Steve', 'Clark', 'Brian', 'Oren', 'Steve' ]
```