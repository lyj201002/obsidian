切换单位：
- shift + e 切换内存显示单位(k,m,g,t,p)
- e 切换进程显示单位(k,m,g,t,p)

排序：
- shift + p 进程按cpu使用率排序
- shift + m 进程按内存使用率排序
- shift + t 按使用cpu积累事件排序

翻页：
- shift + < 向前翻页
- shift + > 向后翻页

CPU：
- t 显示cpu状态
- 1 显示每个cpu状态

显示设置：
- c 显示完整命令
- f 进入设置页面
> - q/esc 退出设置页面
> up/down 上下选择列名，被选中的列名可执行设置操作
> - space或d 选择/取消显示选中的列
> - s 按选中的列排序
