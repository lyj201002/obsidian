# Chapter 1
## Reinforcement Learning
- exploration vs.  exploitation
为获得最大收益(reward)，智能体倾向于选择过去为它产生收益的动作(action)。但为发现这些动作又必须要选择过去未尝试过的动作。
> The agent has to exploit what it has already experienced in order to obtain reward, but it also has to explore in order to make better action selections in the future. 

## Elements of Reinforcement Learning
- policy
>  the  learning  agent’s  way  of  behaving  at  a  given  time.Roughly  speaking,  a  policy  is  a  **mapping  from  perceived  states  of  the  envi-ronment to actions** to be taken when in those states. 
- reward
> On each time step, the environment sends to the reinforcement learning agent a single number
- value
> the **total** amount of reward an agent can expect to accumulate over the future, starting from that state
- model
> mimics the behavior of the environment