---
aliases: UCB
---

# 置信度上界的动作选择

$$A_t = \arg \max \limits_{a} \Big[ Q_t(a)\ +\ c\sqrt{\frac{\ln{t}}{N_t(a)}} \Big]$$

$A_{t}$ 为$t$时刻选择的动作
$Q_{t}(a)$ 为$t$时刻时选择$a$动作带来的价值估计(estimated value of action a)
$N_t(a)$为到$t$时刻为止选择动作$a$的次数
