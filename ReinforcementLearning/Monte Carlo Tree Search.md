---
aliases:mcts
---

## 蒙特卡洛树搜索
---
- Selection
- Expansion
- Simulation
- Back-Propagation

![[v2-b958662a0be8daf52bea1cd735a7575c_r.jpg|300]]

> 以两个玩家的游戏为例，图中不同的颜色代表玩家双方，对于其中一方游戏winning，另一方必为failing。

### Selection
可以将节点分为三类：
- 未访问：未评估过当前state
- 未完全展开：至少评估过一次当前state，但是子节点没有完全评估过
- 完全展开：子节点全部评估过

Selection按照特定的policy([[Upper-Confidence-Bound|UCB]])选择子节点，直到访问到未完全展开节点或未访问节点。


![[mcts_selection.png|300]]
> 分母表示节点访问次数，分子为winning次数
> 对于深色节点，winning即意味着浅色节点failing
> 以左侧第一个节点为例，共访问3次，分别到达左侧3个叶节点。winning 2次，分别为左侧第一个浅色节点(failing，意味着深色winning)、左侧第三个深色节点(winning)

### Expansion
当子节点未完全展开时，无法再继续向下选择。可随机或$\epsilon$-greedy，从所有子节点中选择一个(对于未访问的子节点初始化winning次数、访问次数为0)

![[mcts_expansion.png|300]]

### Simulation
在Expansion后及逆行Monte Carlo Simulation，可使用随机算法或启发式算法随机博弈(可进行到博弈终点，也可指定博弈深度)，最终得到simulation结果(winning or failing)

![[mcts_simulation.png|300]]

### Back-Propagation

当simulation结束后反向更新 Selection、Expansion路径上的节点(访问次数加1，winning or failing 次数相应增减)

![[mcts_backprop.png|300]]

> simulation放回结果为winning，因此expansion结果(浅色节点)加1，其父节点不变，祖父节点结果加1以此类推。

---
### 资料
[Introduction to Monte Carlo Tree Search](https://jeffbradberry.com/posts/2015/09/intro-to-monte-carlo-tree-search/)
