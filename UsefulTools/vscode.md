> 多光标
- <kbd>alt</kbd> + 左键 设置多光标
- <kbd>shift</kbd> + <kbd>alt</kbd> + 左键移动 选择一列
- <kbd>ctrl</kbd> + <kbd>alt</kbd> + <kbd>up</kbd>/<kbd>down</kbd> 添加多光标
- <kbd>ctrl</kbd> + d 选中光标所选内容，或下一个与光标所选相同内容
- <kbd>ctrl</kbd> + <kbd>shift</kbd> + <kbd>L</kbd> 在所有与光标所选内容相同处添加光标
- <kbd>shift</kbd> + <kbd>ctrl</kbd> + <kbd>alt</kbd> + <kbd>left</kbd>/<kbd>right</kbd> 左右列选择





---
> 编辑
- <kbd>shift</kbd> + <kbd>alt</kbd> + <kbd>up</kbd>/<kbd>down</kbd> 在光标前/后粘贴选中内容
- <kbd>alt</kbd> + <kbd>up</kbd>/<kbd>down</kbd> 上/下移行

---
> 选择
- <kbd>shift</kbd> + <kbd>ctrl</kbd> + <kbd>left</kbd>/<kbd>right</kbd>/<kbd>up</kbd>/<kbd>down</kbd> 上下左右选择
- <kbd>ctrl</kbd> + <kbd>L</kbd> 选择一行
---
> 文件
- <kbd>ctrl</kbd> + <kbd>tab</kbd> 打开文件历史记录
- <kbd>alt</kbd> + <kbd>left</kbd>/<kbd>right</kbd> 历史文件切换


---
> remote

免密登录：
- 本地执行`ssh-keygen -b 4096 -t rsa`生成密钥
- 查看服务器是否有`~/.ssh/authorized_keys`文件
- 若没有则新建，并更改权限`chmod 600 ~/.ssh/authorized_keys`
- 将公钥复制到服务器`authorized_keys`文件中
