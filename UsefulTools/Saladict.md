1. Chrome 安装沙拉查词插件
	- 打开“基本选项”-“后台保持运行”
	- 打开“权限管理”-“读取、写入剪切板”
	- “PDF设置”- [浏览器外划词](https://saladict.crimx.com/native.html)
2. 安装[Quicker](https://github.com/crimx/ext-saladict/issues/493)
3. [复制动作](https://getquicker.net/sharedaction?code=b0d1a134-8284-4a44-d1be-08d746da5869)， 中键打开Quicker，右键“粘贴分享的动作”
4. 设置
	- chrome 地址栏输入chrome://extension，进入键盘快捷键。设置沙拉查词“在独立窗口中搜索剪切板内容”，快捷键为Alt + L，范围为全局
	- 打开Quicker设置，打开左键辅助
	- 打开Quicker - 右键“文本/截图翻译”-“动作设置”-“设置面板”，选择翻译方式(划词翻译、复制翻译、截图翻译)
5. 设置沙拉查词置顶显示
	- 复制[置顶命令](https://getquicker.net/sharedaction?code=03ddc264-89d4-446a-4398-08d7f4f4652e)，在Quicker中安装