# Marp: Markdown Presentation Ecosystem
---
`marp`这款工具可以让我不用太在意样式，关注地以markdown格式编写ppt的内容，写完之后简单地分隔一下，对样式作稍许调整，一个精美的ppt就形成了。


下载个VSCode, 天然支持 `markdown` ，然后在左侧的插件栏中搜索并安装 `Marp for VS Code` 就可以开始了。为了获得更好的 Markdown 编辑体验，大家不妨再安装一个叫做 `Markdown All in One` 的插件。



---
使用 `Markdown` 输出一份最简单的幻灯片，只需要让编辑器知道两点即可：它是幻灯片（不是文档）以及它该在哪里分页，通过如下代码做到：
```text
---
marp: true
---
幻灯片1

1. asdf
2. asdf
---
幻灯片2

* asdf
* asdf
```
编辑完成后，通过编辑器右上角的 `Marp` 图标按钮就可以调出 `Export slide deck...` 命令并导出幻灯片了。 `Marp` 插件目前支持导出 `HTML` 和 `PDF` 格式，另外可以将首页导出为 `PNG` 或 `JPEG` 格式的图片。

---
# Marpit Markdown
> Marpit Markdown syntax focuses a compatibility with commonly Markdown documents. It means the result of rendering keeps looking nice even if you open the Marpit Markdown in a general Markdown editor.

## 创建Slides
使用分隔符`---`分割不同页，分隔符前须有一空行。若不想添加空行可使用`___`、`***`或`- - -`作为分隔符

## Directives
> Marpit Markdown has extended syntax called **“Directives”** to support writing awesome slides. It can control your slide-deck theme, page number, header, footer, style, and so on.
> directives 应写成YAML格式

#### HTML comment格式
```
<!-- 
theme: default 
paginate: true 
-->
```
> 使用YAML格式的directives，被注释后不会被解释为注释
#### Front-matter 格式
```
---
theme: default
paginate: true
---
```
> 必须放在最开始位置，并用分隔符包裹

###  Type of Directives
#### 全局指令
- theme
- style
```
---
theme: base-theme
style: |
  section {
    background-color: #ccc;
  }
---
```
- headingDivider
> 定义后可根据`markdown`标题自动分割页面

```
<!-- headingDivider: 2 -->

# 1st page

The content of 1st page

## 2nd page

### The content of 2nd page

Hello, world!

# 3rd page

😃

```
如上， 定义了分割等级为`2`，则一级标题、二级标题会被认为是页面分割，结果如下

![[Pasted image 20210425122402.png]]

必要时可定义[`$`](https://marpit.marp.app/directives?id=custom-directives)作为全局指令命令

#### 局部指令
- paginate 为True 显示页码
- header 页眉内容
- footer 页脚内容
- backgroundColor 背景颜色
```
#渐变色
<!-- backgroundImage: "linear-gradient(to bottom, #67b8e3, #0288d1)" -->
```
- backgroundImage 背景图片
- color  文本颜色
- class 使用特定class渲染
```
section.lead h1 {
  text-align: center;
}
```
使用lead类可使文本居中
```
<!-- _class: lead -->

# THE LEADING HEADER

```
- backgroundPosition
- backgroundRepeat
- backgroundSize

> `<!-- backgroundColor: aqua -->`
每页设置的指令会作用域当前页及之后页

> 使用`_`前缀可只作用域当前页
> ```
> <!-- _backgroundColor: aqua -->
> 
> Add underscore prefix `_` to the name of local directives.
> 
> ---
> 
> The second page would not apply setting of directives.
> ```
![](https://marpit.marp.app/assets/directives.png)

## Image syntax
### Resizing
使用 `width`和 `height`命令调整图片尺寸
```
![width:200px](image.jpg) <!-- Setting width to 200px -->
![height:30cm](image.jpg) <!-- Setting height to 300px -->
![width:200px height:30cm](image.jpg) <!-- Setting both lengths -->
![w:32 h:32](image.jpg) <!-- Setting size to 32x32 px -->
```
### Filters
![[Pasted image 20210425124110.png]]
也可同时使用多个filters
```
![brightness:.8 sepia:50%](https://example.com/image.jpg)
```
### BackGrounds
当设置了多个bg时，采用最后一个
```
![bg](https://example.com/background.jpg)
```
可设置bg图片尺寸，命令与`background-size`相同
```
![bg contain](https://example.com/background.jpg)
```
![[Pasted image 20210425124436.png]]
#### Multiple background
```
![bg](https://fakeimg.pl/800x600/0288d1/fff/?text=A)
![bg](https://fakeimg.pl/800x600/02669d/fff/?text=B)
![bg](https://fakeimg.pl/800x600/67b8e3/fff/?text=C)
```
![Multiple backgrounds|300](https://raw.githubusercontent.com/marp-team/marpit/main/docs/assets/image-syntax/multiple-bg.png)
#### Direction 
```
![bg vertical](https://fakeimg.pl/800x600/0288d1/fff/?text=A)
![bg](https://fakeimg.pl/800x600/02669d/fff/?text=B)
![bg](https://fakeimg.pl/800x600/67b8e3/fff/?text=C)
```
![Multiple backgrounds with vertical direction|300](https://raw.githubusercontent.com/marp-team/marpit/main/docs/assets/image-syntax/multiple-bg-vertical.png)
#### Split backgrounds
使用`left`或`right`命令指定背景位置
```
![bg left](https://picsum.photos/720?image=29)

# Split backgrounds

The space of a slide content will shrink to the right side.
```
![Split backgrounds|300](https://raw.githubusercontent.com/marp-team/marpit/main/docs/assets/image-syntax/split-background.jpg)

也可使用多背景
```
![bg right](https://picsum.photos/720?image=3)
![bg](https://picsum.photos/720?image=20)

# Split + Multiple BGs

The space of a slide content will shrink to the left side.
```
先指定背景在右侧，然后使用多背景
![Split + Multiple BGs|300](https://raw.githubusercontent.com/marp-team/marpit/main/docs/assets/image-syntax/split-multiple-bg.jpg)
split时也可指定占用比例
```
![bg left:33%](https://picsum.photos/720?image=27)

# Split backgrounds with specified size

```
![Split backgrounds with specified size|300](https://raw.githubusercontent.com/marp-team/marpit/main/docs/assets/image-syntax/split-bg-with-size.jpg)
#### Shorthand for setting colors
```
# Hex color (White BG + Black text)

![bg](#fff)
![](#000)

---

# Named color (rebeccapurple BG + White text)

![bg](rebeccapurple)
![](white)

---

# RGB values (Orange BG + White text)

![bg](rgb(255,128,0))
![](rgb(255,255,255))
```
## Fragmented list
无须列表使用`-`、`+`、`*`
有序列表使用`.`、`)`









## 优化样式
默认slide尺寸为`4:3`(960x720)

有三款主题可以选择，可以参考[themes](https://github.com/marp-team/marp-core/tree/main/themes)
> 明暗风格可使用`invert`命令切换
> ```
> <!-- class: invert \-->
> ```
- default
![](https://user-images.githubusercontent.com/3993388/48039490-53be1b80-e1b8-11e8-8179-0e6c11d285e2.png)![](https://user-images.githubusercontent.com/3993388/48039492-5456b200-e1b8-11e8-9975-c9e4029d9036.png)
- gaia
![](https://user-images.githubusercontent.com/3993388/48039493-5456b200-e1b8-11e8-9c49-dd5d66d76c0d.png)![](https://user-images.githubusercontent.com/3993388/48039494-5456b200-e1b8-11e8-8bb5-f4a250e902e1.png)
- uncover
![](https://user-images.githubusercontent.com/3993388/48039495-5456b200-e1b8-11e8-8c82-ca7f7842b34d.png)![](https://user-images.githubusercontent.com/3993388/48039496-54ef4880-e1b8-11e8-9c22-f3309b101e3c.png)