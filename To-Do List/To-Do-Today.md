### 2021.4.17
---
#### kaggle script
- [x] https://www.kaggle.com/phunghieu/connectx-with-deep-q-learning-pytorch#Train-the-agent connect X with pytorch DQN
- [ ] https://www.kaggle.com/yanamal/approximate-q-learning-baseline geese approximate Q-Learning
- [ ] https://www.kaggle.com/fleek12/hungry-geese-worldmodels-pytorch?scriptVersionId=57321518 geese pytorch worldmodels
- [x] https://www.kaggle.com/ks2019/handy-rl-training-notebook#Preparing-Submission-File HandyRL training
- [x] https://www.kaggle.com/nejumi/let-s-create-your-agent-by-supervised-learning/ keras training supervised learning

#### coding
- [ ] HandyRL source code
- [ ]  gym source code

---
### 2021.4.18
#### coding
- [ ] HandyRL source code
> - [x] m.pyain
> - [ ] agent.py
> - [ ] connection.py
> - [ ] environment.py
> - [ ] generation.py
> - [ ] losses.py
> - [ ] model.py
> - [ ] util.py
> - [ ] worker.py
> - [ ] hungry_geese.py
- [ ]  gym source code

###2021.4.19
#### coding
- [ ] HandyRL source code
> - [x] m.pyain
> - [ ] agent.py
> - [ ] connection.py
> - [ ] environment.py
> - [x] generation.py
> - [ ] losses.py
> - [ ] model.py
> - [ ] util.py
> - [ ] worker.py
> - [ ] hungry_geese.py

###2021.4.20
#### coding
- [ ] HandyRL source code
> - [x] main.py
> - [x] agent.py
> - [ ] connection.py
> - [x] environment.py
> - [x] generation.py
> - [ ] losses.py
> - [x] model.py
> - [x] util.py
> - [x] worker.py
> - [x] hungry_geese.py
> - [ ] train.py

###2021.4.21
#### coding
- [ ] HandyRL source code
> - [x] main.py
> - [x] agent.py
> - [x] connection.py
> - [x] environment.py
> - [x] generation.py
> - [ ] losses.py
> - [x] model.py
> - [x] util.py
> - [x] worker.py
> - [x] hungry_geese.py
> - [ ] train.py

###2021.4.22
#### coding
- [ ] HandyRL source code
> - [x] main.py
> - [x] agent.py
> - [x] connection.py
> - [x] environment.py
> - [x] generation.py
> - [ ] losses.py
> - [x] model.py
> - [x] util.py
> - [x] worker.py
> - [x] hungry_geese.py
> - [x] train.py

### 2021.4.28
强化学习 第九章

### 2021.5.2

- [ ] 完成输入centerlize
- [ ] 查清为何generation产生的episode胜率为0.5

