
## installation
```
 pip install kaggle
 enter kaggle account and download api cookie 
to C:\Users\<Windows-username>\.kaggle\kaggle.json
```
---
## API
```
kaggle competitions list
kaggle competitions list --sort-by prize
kaggle competitions list --csv > competitions_list.csv   #保存在csv文件中
kaggle competitions leaderboard -s facebook-ii   #查看排行榜
kaggle datasets list -s [KEYWORD]   #按关键词搜索数据集
kaggle datasets download -d [DATASET]
kaggle competitions download -c [COMPETITION]: download files associated with a competition
kaggle competitions submit -c [COMPETITION] -f [FILE] -m [MESSAGE]: make a competition submission



```