- [x] Mastering the Game of Go without Human Knowledge			#AlphaGo-Zero 


![[Mastering the Game of Go without Human Knowledge.pdf]]

- [x] Mastering the game of Go with deep neural networks and tree search			#AlphaGo 


![[PaperLists/src/nature16961.pdf]]

- [x] ALPHAGO ZERO: A NOTE WITH PSEUDO CODE

![[AlphaGoZeroPseudoCode.pdf]]

- [ ] Reinforcement learning improves behaviour from evaluative feedback

![[Reinforcement learning improves behaviour from evaluative feedback.pdf]]

- [ ] Human-level control through deep reinforcement learning


![[Human-level control through deep reinforcementlearning 1.pdf]]

- [ ] Deep Reinforcement Learning: An Overview

![[DEEPREINFORCEMENTLEARNING ANOVERVIEW.pdf]]